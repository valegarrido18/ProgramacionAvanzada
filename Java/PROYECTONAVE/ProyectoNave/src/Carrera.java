import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Carrera {
	Nave nave;
	Ambulancia ambulancia = new Ambulancia();
	Scanner entrada = new Scanner(System.in);
	// ArrayList para agregar n naves.
	private ArrayList<Nave> naves = new ArrayList<Nave>(); 
	
	public void llenar_carrera(){
		int num_naves = 0;
		int opcion;
		
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println("Elija la cantidad de naves a competir: ");
		System.out.println(" [1] Contra dos naves \n [2] Contra cuatro naves \n [3] Contra seis naves");
		opcion = entrada.nextInt();
		
		// Contrincante más usuario.
		
		if (opcion == 1) {
			num_naves = 3;
		}
		
		else if (opcion == 2) {
			num_naves = 5;
		}
		
		else if (opcion == 3) {
			num_naves = 7;
		}
		
		else {
			System.out.println("ERROR: Selección inválida! \n Inténtelo nuevamente \n");
			llenar_carrera();
		}

		for (int i=0; i<num_naves; i++) { 
			// Se instancian las naves.
			nave = new Nave(); 
			nave.setNombre(i+1);

			nave.ensamble();
			// Se agregan las naves al ArrayList.
			naves.add(nave); 
		}
		
		nave.alas.colores();
		partida();
		
		for (Nave nave: naves) {
			// Se mantiene hasta finalizar la carrera.
			while(nave.odometro.getDistancia_final() != 0) { 
				nave.calculo();
				
				if (nave.getDeterioro() <= 60) {
					ambulancia.arreglo_deterioro();
					nave.propulsor.setAceleracion(0);
					nave.estanque.setCantidad_combustible(100);
					nave.setDeterioro(100);
					nave.calculo();
					imprimir_recorrido(nave.odometro.getDistancia_final());
				}
				pausa();
				System.out.println("_______________________________________________________________");
				System.out.print("\033[H\033[2J");
				System.out.flush();
			}	
		}
		
		if (nave.odometro.getDistancia_final() == 0) {
			salida();
			Collections.sort(naves);
			int posiciones = 0;
			int nombre_nave;
			String reset = "\u001B[0m";
			
			System.out.println("Posiciones:");
			for (Nave nave: naves) {
				posiciones += 1;
				nombre_nave = nave.getNombre();
				System.out.println(" Lugar " + posiciones + "         " + nave.getColor() + "Nave " + nombre_nave + reset );
				}
			}
		}
		
		public static void imprimir_recorrido (int recorrido) {
			if (recorrido > 0) {
				System.out.print("*");
				imprimir_recorrido(recorrido - 1);
			}
		}

	
	public void pausa() {
	    try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}
	
	
	public void partida() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println(          "   *******   *******   *   *         ");
		System.out.println(          "   **        **    *     *  *        ");
		System.out.println(          "   ** ****   **    *      *  *       ");
		System.out.println(          "   **   **   **    *     *  *        ");
		System.out.println(          "   *******   *******    *  *         "); 
		
		pausa();
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}
	
	public void salida() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
		System.out.println(          "   ******  **  *    *  **  ******  **   **	       ");
		System.out.println(          "   **      **  **   *  **  *       **   **		   ");
		System.out.println(          "   ****    **  * *  *  **  ******  *******  		   ");
		System.out.println(          "   **      **  *  * *  **       *  **   **	       ");
		System.out.println(          "   **      **  *    *  **  ******  **   **		   ");
		System.out.println("");
	}
}
