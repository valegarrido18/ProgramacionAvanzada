public class Nave implements InterfazNave,  Comparable<Nave> {
	// Creación de objetos.
	Estanque estanque;
	Odometro odometro;
	Propulsor propulsor; 
	Alas alas; 
	
	// Variables
	private int deterioro = 100;
	private int nombre;
	private int tiempo_actual = 0;
	private	int velocidad_actual = 0;
	private int distancia_actual = 0;
	private String color;
	
	Nave(){
		alas = new Alas();
		estanque = new Estanque();
		odometro = new Odometro();
	    propulsor = new Propulsor();
	}
	
	public void ensamble() {
		

		estanque.combustible();
		odometro.tiempo_aleatorio(1,30);
	}
		
	public void calculo() {
		int distancia;
		
		crear_nombre();
		// Determina movimiento de la nave.
		if (velocidad_actual < 1000) {
			velocidad_actual += propulsor.getAceleracion();
		}
		// La nave alcanza su velocidad máxima.
		else {
			velocidad_actual = 1000;
			propulsor.setAceleracion(0);
		}
	 	  
	    System.out.println("Tiempo: " + tiempo_actual);	
		System.out.println("Velocidad: " + velocidad_actual);
	    
	    propulsor.calculo_aceleracion(velocidad_actual, 1000, tiempo_actual,odometro.getTiempo(), estanque.getTipo_combustible());
	    System.out.println("Aceleracion: " + propulsor.getAceleracion());
	    
	    odometro.calculo_distancia(getDistancia_actual());
	    System.out.println("Distancia: " + odometro.getDistancia_final());
	    
	    distancia = getTiempo_actual() * getVelocidad_actual();
	    distancia_actual += distancia;
	    setDistancia_actual(distancia_actual);
	    
	    tiempo_actual += odometro.getTiempo();
	    setTiempo_actual(tiempo_actual);
	    
	    System.out.println("");
	    multiplo_velocidad(velocidad_actual);
	}
	
	// Por cada multiplo de 5 en la velocidad, hay deterioro.
	public void multiplo_velocidad(int velocidad_actual) { 
 		int resto;
		int numero1 = velocidad_actual;
		int numero2 = 5;
		 
		resto = numero1 % numero2;
		 
		if (resto == 0) {
			deterioro();
		}
	}
	
	// Evalúa el tipo de deterioro.
	public void deterioro() { 
		int tipo_deterioro;
		
		tipo_deterioro = (int) (Math.random() * 4) + 1;
		
		if (tipo_deterioro == 1) {
			estanque.deterioro_combustible();
			if(estanque.getCantidad_combustible() == 0) {
				System.out.println("Se acabo el combustible");
			}

			else {
				System.out.println("El combustible se esta acabando, queda " + estanque.getCantidad_combustible() + "%");
				deterioro -= 30;
			}	
		}
		
		else if (tipo_deterioro == 2) {
			System.out.println("ALERTA! Falla en el propulsor\n");
			deterioro -= 15; 
			System.out.println("El propulsor se dañó a un " + deterioro + "%");
		}
		
		else if (tipo_deterioro == 3) {
			System.out.println("ALERTA! Falla en las alas de la nave\n");
			deterioro -= 20;
			System.out.println("Las alas se dañaron a un " + deterioro + "%");
		}
		
		else {
			System.out.println("ALERTA! Falla en el odómetro\n");
			deterioro -= 25; 
			System.out.println("El odómetro se dañó a un " + deterioro + "%");
		}
	}

	// Le da un color distinto a la nave del usuario para identificarlo. 
	public void crear_nombre() { 
 		String verde = "\u001B[42m";
		String reset = "\u001B[0m";
		String morado = "\u001B[45m";
		String celeste = "\u001B[46m";
		String blanco = "\u001B[37m";

		if (alas.getColor_alas() == 1) {
			color = celeste;
		}
	
		else if(alas.getColor_alas() == 2) {
			color = verde;
		}
		
		else if(alas.getColor_alas() == 3) {
			color = morado;
		}
		
		else{
			color = blanco;
		}
			
		System.out.println(color + "Nave " + nombre + reset);
	
	}
	
	 public int compareTo(Nave i) {
	        int resultado = 0;
	        if(this.tiempo_actual > i.tiempo_actual){
	        	resultado =+1;
	        }
	        else if (this.tiempo_actual <i.tiempo_actual){
	        	resultado =- 1;
	        }
	        return resultado;
	    }

	
	// Getters y Setters
	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public int getTiempo_actual() {
		return tiempo_actual;
	}

	public void setTiempo_actual(int tiempo_actual) {
		this.tiempo_actual = tiempo_actual;
	}

	public int getVelocidad_actual() {
		return velocidad_actual;
	}

	public void setVelocidad_actual(int velocidad_actual) {
		this.velocidad_actual = velocidad_actual;
	}

	public int getDistancia_actual() {
		return distancia_actual;
	}

	public void setDistancia_actual(int distancia_actual) {
		this.distancia_actual = distancia_actual;
	}

	public int getDeterioro() {
		return deterioro;
	}

	public void setDeterioro(int deterioro) {
		this.deterioro = deterioro;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}