import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		int opcion;
		
		Juego juego = new Juego();
		
		System.out.println("METODOS DE JUEGOS");
		System.out.println(">>Kachipun Simple  (1)");
		System.out.println(">>Kachipun Doble   (2) ");
		
		Scanner entrada = new Scanner(System.in);
	
		opcion = entrada.nextInt();
		
		if ( opcion == 1 ) {
			juego.kchipun();
		}
		
		else if ( opcion == 2 ) {
			juego.kchipunDoble();  
		}
		else {
			System.out.println("Opción inválida");
		}
		
		entrada.close();
		
	}	
}
