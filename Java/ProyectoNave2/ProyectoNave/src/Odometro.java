public class Odometro {
	private int tiempo;
	public int nivel_deterioro;
	private int distancia_final=10000;
	
	// Asigna un valor aleatorio al tiempo.
	public void tiempo_aleatorio(int minimo, int maximo) { 
		int aleatorio;
		aleatorio =  (int) (Math.random() * maximo) + minimo; 
		tiempo = aleatorio;
		setTiempo(tiempo);
	}
	 
	public void calculo_distancia(int distancia) {                  
		distancia_final -= distancia; 
		
		if (distancia_final < 0) { 
			distancia_final = 0; 
		}
	}

	// Getters y Setters
	public int getTiempo() {
		return tiempo;
	}
	
	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public int getDistancia_final() {
		return distancia_final;
	}

	public void setDistancia_final(int distancia_final) {
		this.distancia_final = distancia_final;
	}
	
}
