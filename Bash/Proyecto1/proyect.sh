#!/bin/bash -x

function MENU_COLUMNAS(){

echo
echo
echo " MENU >> INTERACCION CON ARCHIVO PDB"  
echo
echo "--  (1)  Ver primera columna      --"
echo "--  (2)  Ver segunda columna      --"
echo "--  (3)  Ver tercera columna      --"
echo "--  (4)  Ver cuarta columna       --"
echo "--  (5)  Ver quinta columna       --"
echo "--  (6)  Ver sexta columna        --"
echo 
echo "--  (A)  VOLVER ATRAS             --"
echo "--  (B)  MENU PRINCIPAL           --"
echo 
echo "------------------------------------"

read -p "--> Ingrese opción: " columna 
    
if [[ $columna == 1 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $1}'
    
    volver1=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver1
    clear
    
    if [[ $volver1 == "s" ]] || [[ $volver1 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver1 == "n" ]] || [[ $volver1 == "N" ]]; then
        exit 1
    fi
    
elif [[ $columna == 2 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $2}' 

    volver2=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver2
    clear
    
    if [[ $volver2 == "s" ]] || [[ $volver2 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver2 == "n" ]] || [[ $volver2 == "N" ]]; then
        exit 1
    fi

elif [[ $columna == 3 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $3}' 

    volver3=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver3
    clear
    
    if [[ $volver3 == "s" ]] || [[ $volver3 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver3 == "n" ]] || [[ $volver3 == "N" ]]; then
        exit 1
    fi

elif [[ $columna == 4 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $4}' 

    volver4=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver4
    clear
    
    if [[ $volver4 == "s" ]] || [[ $volver4 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver4 == "n" ]] || [[ $volver4 == "N" ]]; then
        exit 1
    fi

elif [[ $columna == 5 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $5}' 

    volver5=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver5
    clear
    
    if [[ $volver5 == "s" ]] || [[ $volver5 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver5 == "n" ]] || [[ $volver5 == "N" ]]; then
        exit 1
    fi

elif [[ $columna == 6 ]]; then 
    cat $PROTEINA.pdb | grep ^ATOM | awk '{print $6}' 

    volver6=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver6
    clear
    
    if [[ $volver6 == "s" ]] || [[ $volver6 == "S" ]]; then
        MENU_COLUMNAS
    fi

    if [[ $volver6 == "n" ]] || [[ $volver6 == "N" ]]; then
        exit 1
    fi

elif [[ $columna == "a" ]] || [[ $columna == "A" ]]; then
    clear
    MENU_ATOM

elif [[ $columna == "b" ]] || [[ $columna == "B" ]]; then
    clear
    MENU

else 
    clear
    echo "ERROR: -- INGRESE UNA OPCIÓN VÁLIDA --"
    MENU_COLUMNAS
fi
}


# Función para el manejo del archivo pdb 
function MENU_ATOM(){

echo
echo  
echo "             MENU >> ARCHIVO PDB                 " 
echo 
echo "--  (1)  Contenido pdb completo                --"
echo "--  (2)  Solo contenido ATOM                   --"
echo "--  (3)  Distribucion de archivo por columnas  --"
echo 
echo "--  (A)  Volver al menu                        --"
echo "--  (B)  SALIR                                 --"
echo
echo "-------------------------------------------------" 

read -p "--> Ingrese su opción:  " opcion

if [[ $opcion == 1 ]]; then     
    cat $PROTEINA.pdb
    
    volver1=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver1
    clear
    
    if [[ $volver1 == "s" ]] || [[ $volver1 == "S" ]]; then
        MENU_ATOM
    fi

    if [[ $volver1 == "n" ]] || [[ $volver1 == "N" ]]; then
        exit 1
    fi

elif [[ $opcion == 2 ]]; then
    cat $PROTEINA.pdb | grep ^ATOM 
    
    volver2=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver2
    clear
    
    if [[ $volver2 == "s" ]] || [[ $volver2 == "S" ]]; then
        MENU_ATOM
    fi

    if [[ $volver2 == "n" ]] || [[ $volver2 == "N" ]]; then
        exit 1
    fi    

elif [[ $opcion == 3 ]]; then
    clear
    MENU_COLUMNAS

elif [[ $opcion == "a" ]] || [[ $opcion == "A" ]]; then
    clear
    MENU

elif [[ $opcion == "b" ]] || [[ $opcion == "B" ]]; then
    exit 1 
else 
    clear
    echo "ERROR: -- INGRESE UNA OPCIÓN VÁLIDA --"
    MENU_ATOM
fi 
}


# Función para verificar proteínas 
function verificando_archivo(){ 
    if [ -f $PROTEINA.pdb ]; then
        clear
        echo "-- PROTEÍNA YA DESCARGADA --" 
        MENU
    else
        if ping -c 1 google.com; then
            for i in $( cat bd-pdb.txt | grep \"Protein\" | cut -c 2-5 ); do
                if [[ $PROTEINA == $i ]]; then
                    wget https://files.rcsb.org/download/$PROTEINA.pdb #descargar proteina de pdb
                    clear 
                    echo "-- DESCARGA COMPLETA --"
                    MENU
                fi     
            done
        else
            echo "ERROR: -- SIN CONEXIÓN A INTERNET --"
        fi
    fi
}


# Función para revisar que la base de datos exista, de no ser así se usará el link para descargar. 
function down_base(){
    for (( c=1; c<=3; c++)); do
    echo "--> Revisando base de datos...  "

    done
    if [ -f bd-pdb.txt ]; then 
        verificando_archivo
    else  
        wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        verificando_archivo
    fi
}


# Función para llamar el menú
function MENU(){
echo 
echo "------------------------------------------------------------------------"
echo
echo "--                                MENU                                --"
echo "--                Descargar proteína ingresada     (A)                --"
echo "--                Contenido PDB                    (B)                --"
echo "--                Base de datos solo de proteínas  (C)                --"
echo "--                Descargar otra proteína          (D)                --"
echo "--                SALIR                            (E)                --"
echo
echo "------------------------------------------------------------------------"
 
read -p " --> Ingrese su opción: " opcion

# Descargar proteína 
if [[ $opcion == "a" ]] || [[ $opcion == "A" ]]; then
    down_base

# Contenido proteína
elif [[ $opcion == "b" ]] || [[ $opcion == "B" ]]; then
    clear
    MENU_ATOM

# Ver base de datos
elif [[ $opcion == "c" ]] || [[ $opcion == "C" ]]; then
    cat bd-pdb.txt | grep \"Protein\" | cut -c 2-5

    volver=" "    
    read -p "--> DESEA VOLVER? S/N:  " volver
    clear
    if [[ $volver == "s" ]] || [[ $volver == "S" ]]; then
        MENU
    fi

    if [[ $volver == "n" ]] || [[ $volver == "N" ]]; then
        exit 1
    fi 

# Proteína Nueva
elif [[ $opcion == "d" ]] || [[ $opcion == "D" ]]; then
    nueva=" "
    PROTEINA=" " 
    read -p " --> Ingrese nueva proteína: " nueva
    nueva="${nueva^^}"
    PROTEINA=$nueva
    verificando_archivo

# Salir
elif [[ $opcion == "e" ]] || [[ $opcion == "E" ]]; then
    exit 1
else 
    clear
    echo "ERROR: -- INGRESE UNA OPCIÓN VÁLIDA --"
    MENU
fi
}


PROTEINA="${1^^}"

if [[ $# > 1 ]]; then # ver que solo se ingrese un parámetro
    echo "ERROR: -- Solo se admite un parámetro --" 
    echo $PROTEINA
    exit 1
fi
 
if [[ $# == 1 ]]; then # parámetro que se utiliza
    if [ -f bd-pdb.txt ]; then 
        valor=$(cat bd-pdb.txt | grep \"$PROTEINA\" | wc -l)
        clear
        if [[ $valor == 1 ]]; then
            MENU
        else     
            echo "ERROR: -- Parámetro inválido --"
        fi
    else  
        wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        MENU 
    fi
fi   
    
if [[ $# == 0 ]]; then # error para cuando no se ingrese ningún parámetro
    echo "ERROR: -- Debe ingresar un parámetro --"
    exit 1
fi

