#!/bin/bash

PARAMETRO=$1

without_param () {
    echo >&2 "$@"
    exit 1
}

if [[ $# -ne 1 ]]; then
    echo "NO HAY PARAMETRO"
fi

if [[ $# -eq 1 ]]; then
    ls -lSr /$HOME/$PARAMETRO | awk -F " " '{print $9}'| nl
fi 
