#!/bin/bash -x

read -p "Ingresa un caracter: " CARACTER
echo "El caracter es: '$CARACTER'"

if [[ $CARACTER =~ ^[0-9]+$ ]]; then 
    echo "CORRESPONDE A UN NUMERO"

elif [[ $CARACTER =~ ^[a-zA-Z]+$ ]]; then 
    echo "CORRESPONDE A UNA LETRA"

else 
    echo "CORRESPONDE A UN CARACTER ESPECIAL"
fi

