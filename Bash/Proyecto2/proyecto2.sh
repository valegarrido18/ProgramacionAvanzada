#!/bin/bash -x

function PDB(){
    echo
    echo "---->  (0) COMPONENTES   "
    echo "---->  (1) ATOM          "
    echo "---->  (2) HETATM        "
    echo "---->  (3) HETATM +HOH   "
    echo "---->  (4) MENU          "
    echo "---->  (5) SALIR         "
    echo

    read -p ">> Ingrese opción: " opcion 
    
    if [[ $opcion == 0 ]]; then 
        clear
        crear_imagen
        cat $PROTEINA.txt

        atras=" "    
        read -p "--> DESEA VOLVER? S/N:  " atras
        clear
        if [[ $atras == "s" ]] || [[ $atras == "S" ]]; then
            PDB
        fi

        if [[ $atras == "n" ]] || [[ $atras == "N" ]]; then
            exit 1
        fi 
    
        elif [[ $opcion == 1 ]]; then
            clear
            cat atom.txt
            atras1=" "    
            read -p "--> DESEA VOLVER? S/N:  " atras1
            clear

        if [[ $atras1 == "s" ]] || [[ $atras1 == "S" ]]; then
            PDB
        fi

        if [[ $atras1 == "n" ]] || [[ $atras1 == "N" ]]; then
            exit 1
        fi 
    
    elif [[ $opcion == 2 ]]; then 
        clear
        cat hetatm.txt
        atras2=" "    
        read -p "--> DESEA VOLVER? S/N:  " atras2
        clear
        if [[ $atras2 == "s" ]] || [[ $atras2 == "S" ]]; then
            PDB
        fi

        if [[ $atras2 == "n" ]] || [[ $atras2 == "N" ]]; then
            exit 1
        fi
 
    elif [[ $opcion == 3 ]]; then 
        cat $PROTEINA.pdb | grep ^HETATM | awk 'length($1) > 6 {$1=substr($1,1,6)" "substr($1,7,11)} {print $0}'
        atras3=" "    
        read -p "--> DESEA VOLVER? S/N:  " atras3
        clear
        if [[ $atras3 == "s" ]] || [[ $atras3 == "S" ]]; then
            PDB
        fi

        if [[ $atras3 == "n" ]] || [[ $atras3 == "N" ]]; then
            exit 1
        fi 

    elif [[ $opcion == 4 ]]; then
        clear 
        MENU

    elif [[ $opcion == 5 ]]; then
        rm atom.txt
        rm hetatm.txt
        rm $PROTEINA.txt     
        exit 1
                                     
    else 
        clear
        echo "ERROR: -- INGRESE UNA OPCIÓN VÁLIDA --"
        PDB
    fi
        
   
}


# Función con archivos temporales para el procedimiento de crear la imagen
function crear_imagen(){     

    # archivo que quita los HOH 
    cat $PROTEINA.pdb | grep ^HETATM | awk 'length($1) > 6 {$1=substr($1,1,6)" "substr($1,7,11)} {print $0}' > tem.txt
    sed "/HOH/d" tem.txt > hetatm.txt
    rm tem.txt

    #read -p "-->> Ingrese distancia: " distancia

    # archivo que solo se queda con los ATOM
    cat $PROTEINA.pdb | grep ^ATOM | awk 'length($3) > 3{$3 = substr($3, 1, 3)" "substr($3, 5, 3)}
                                    length($4) > 3{$4 = substr($4, 2, 3)}{print $0}' >> atom.txt

    cat hetatm.txt | awk -f imagen.awk > $PROTEINA.txt
    #cat hetatm.txt | awk -f caca.awk

}


# Función para verificar proteínas 
function verificando_archivo(){ 

    if [ -f $PROTEINA.pdb ]; then
        clear
        echo "-- PROTEÍNA YA DESCARGADA --" 
        MENU
    else
        if ping -c 1 google.com; then
            for i in $( cat bd-pdb.txt | grep \"Protein\" | cut -c 2-5 ); do
                if [[ $PROTEINA == $i ]]; then
                    wget https://files.rcsb.org/download/$PROTEINA.pdb #descargar proteina de pdb
                    clear 
                    echo "-- DESCARGA COMPLETA --"
                    MENU
                fi     
            done
        else
            echo "ERROR: -- SIN CONEXIÓN A INTERNET --"
        fi
    fi

}


# Función para revisar que la base de datos exista, de no ser así se usará el link para descargar. 
function down_base(){

    for (( c=1; c<=3; c++)); do
    echo "--> Revisando base de datos...  "

    done
    if [ -f bd-pdb.txt ]; then 
        verificando_archivo
    else  
        wget https://icb.utalca.cl/~fduran/bd-pdb.txt
        verificando_archivo
    fi

}


# Función para llamar el menú
function MENU(){
    echo
    echo "-------------------------------------------------------------------------"
    echo "-         >> Distribucion y análisis de datos de archivos PDB <<        -"
    echo "-------------------------------------------------------------------------"
    echo
    echo "--                                MENU                                --"
    echo
    echo "--                Descargar proteína ingresada     (A)                --"
    echo "--                COMPONENTES PROTEÍNA             (B)                --"
    echo "--                Base de datos solo de proteínas  (C)                --"
    echo "--                Descargar otra proteína          (D)                --"
    echo
    echo "--                SALIR                            (X)                --"
    echo
    echo "------------------------------------------------------------------------"
 
    read -p " --> Ingrese su opción: " opcion

    # Descargar proteína 
    if [[ $opcion == "a" ]] || [[ $opcion == "A" ]]; then
        down_base

    elif [[ $opcion == "b" ]] || [[ $opcion == "B" ]]; then
        clear
        crear_imagen
        PDB

    # Ver base de datos
    elif [[ $opcion == "c" ]] || [[ $opcion == "C" ]]; then
        cat bd-pdb.txt | grep \"Protein\" | cut -c 2-5

        volver=" "    
        read -p "--> DESEA VOLVER? S/N:  " volver
        clear
        if [[ $volver == "s" ]] || [[ $volver == "S" ]]; then
            MENU
        fi

        if [[ $volver == "n" ]] || [[ $volver == "N" ]]; then
            exit 1
        fi 

    # Proteína Nueva
    elif [[ $opcion == "d" ]] || [[ $opcion == "D" ]]; then
        nueva=" "
        PROTEINA=" " 
        read -p " --> Ingrese nueva proteína: " nueva
        nueva="${nueva^^}"
        PROTEINA=$nueva
        verificando_archivo

    # Salir 
    elif [[ $opcion == "x" ]] || [[ $opcion == "X" ]]; then
        rm atom.txt
        rm hetatm.txt
        rm $PROTEINA.txt     
        exit 1
    
    else 
        clear
        echo "ERROR: -- INGRESE UNA OPCIÓN VÁLIDA --"
        MENU
    fi
    }   


    PROTEINA="${1^^}"

    if [[ $# > 1 ]]; then # ver que solo se ingrese un parámetro
        echo "ERROR: -- Solo se admite un parámetro --" 
        echo $PROTEINA
        exit 1
    fi
 
    if [[ $# == 1 ]]; then # parámetro que se utiliza
        if [ -f bd-pdb.txt ]; then 
            valor=$(cat bd-pdb.txt | grep \"$PROTEINA\" | wc -l)
            clear
            if [[ $valor == 1 ]]; then
                MENU
            else     
                echo "ERROR: -- Parámetro inválido --"
            fi
        else  
            wget https://icb.utalca.cl/~fduran/bd-pdb.txt
            MENU 
        fi
    fi   
    
    if [[ $# == 0 ]]; then # error para cuando no se ingrese ningún parámetro
        echo "ERROR: -- Debe ingresar un parámetro --"
        exit 1
    fi

