                                                    
						     -- Distribucion y análisis de datos de archivos PDB --

La creación de este proyecto esta enfocada en satisfacer una problematica encontrada, la cual consiste en obtener datos por medio de un archivo el cual se pueda distribuir de varias maneras para entregar un resultado en un formato establecido, entregando tambien al usuario la posibilidad de este conocer dentro de su proteina aquellos que sean ligando y iones.

    • EMPEZANDO

Al ejecutar el script, se deberá hacer junto con el nombre de la proteína que se desea ingresar para la distrubición y análisis, la cual se pasará dentro del código como un parámetro. Al ejecutar, se revisará primero que la base de datos no se encuentre descargada, ya que por lo contrario se descargará; si esta ya está descargada entonces se mostrará el menu del script el cual dará 4 diferentes opciones, es acá donde se comienza una interacción con el usuario sobre la proteína ingresada directamente de PBD, ya que este podrá descargarla, ver su contenido, ingresar a la base de datos para solo ver el nombre de los que si son proteínas debido a que en la base de datos no solo se encuentran proteínas; e incluso se encuentra la opción de volver a descargar una nueva proteína, como tambien existe la opción de solo cerrar el script.
Para esta ejecución es necesario saber que se debe ingresar solo un parámetro, si ingresa dos o más de ellos, se enviará un error informandoselo, lo mismo se hará para cuando no se ingrese ningún parámetro, y ademas, se mostrará el error para cuando el parámetro ingresado no se encuentre en la base de datos y que no sea proteína, porque este scrip esta realizado solo para los archivos que sean unicamente proteínas. 
En fin, el objetivo más general de este script es que el usuario pueda realizar una distribución y analisis de la proteína que desee siempre y cuando se encuentre en la base de datos establecida.

>>La opción agregada la cual corresponde a componentes proteína, abre un nuevo menu el cual da a conocer la distribución del PBD de la proteína ingresada que nos entregará la información de esta en categorías como, los mismos componentes, el cual dentro tiene escificamente cuales son ligando y cuales son iones, además, una  categoría con los ATOM, los HETATM sin HOH y otros con HOH

    • REQUISITOS PREVIOS
     
Sistema operativo Linux
Conexión habilitada a internet

    • INSTALACIÓN

- Para poder clonar directamente el script y hacer funcionalidad de este, se debe hacer directamente desde el siguiente link: 
	https://gitlab.com/valegarrido18/VgarridoRepoSoluciones.git

- Realizamos el código en vim, el cual se instala con el siguiente comando:
	sudo apt-get install vim

*Las descargas del script, tanto la base de datos como las proteinas estan programadas para primero revisar si existen o no, junto con una comprobación a la conexión del internet para realizar la descarga de las proteinas desde la página de PDB por lo que no necesita nada más que solo ejecutar el script adquirido lo anterior. 

-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para la entrada al programa en el editor de texto vim, se necesita del comando: vim nombrearchivo.sh mientras que para ejecutarlo debemos colocar el comando bash que corresponde a bash nombrearchivo.sh

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
BASH: programa informático, cuya función consiste en interpretar órdenes, y un lenguaje de consola.
AWK: lenguaje de programación diseñado para procesar datos basados en texto, ya sean ficheros o flujos de datos
Vim: editor de texto para escribir el código del programa.
Pep-8: la narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

    • ESPECIFICACIONES DE LA PEP-8 EN EL CÓDIGO: 

- Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de cualquier linea presente en el código.

- Después de cada función se dejan dos espacios en blanco de separación al final de una y el comienzo de una distinta. Mientras que por cada metodo dentro de la cada función habra solo una línea de separacion-

- Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
 
- Para cada operador que se utilice en el código, debe haber un espacio en blanco de separación entre ellos. 

Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 

101     cat $PROTEINA.pdb | grep ^ATOM > atom.txt
102 
103     cat hetatm.txt | awk -f imagen.awk > $PROTEINA.txt
104 
105 }
106 
107 
108 # Función para verificar proteínas 
109 function verificando_archivo(){
110 
111     if [ -f $PROTEINA.pdb ]; then
112         clear
113         echo "-- PROTEÍNA YA DESCARGADA --" 
114         MENU
115     else
116         if ping -c 1 google.com; then
117             for i in $( cat bd-pdb.txt | grep \"Protein\" | cut -c 2-5 ); do
118                 if [[ $PROTEINA == $i ]]; then
119


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU bash, versión 4.4.19(1)-release-(x86_64-pc-linux-gnu) 

    • AUTORES

Valentina Garrido - Desarrollo del script, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD
ejemplos utilizados de: https://gitlab.com/fabioduran/programacion_avanzada/tree/master/bash-ejemplos 
proyecto 1 (propuesta): https://gitlab.com/fabioduran/programacion_avanzada/tree/master/unidad1/proyecto1/propuesta
por, Fabio Duran Verdugo.
